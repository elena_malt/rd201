var state = new State();

project.getTag("DayOfMonth",state,-1,function(tagName, state){  project.setTag("Application/GVL_ExportHMI/DateDD_HMI",state.getValue(),0);
                                                                console.log("Day: "+state.getValue());}, false );
project.getTag("Month",     state,-1,function(tagName, state){  project.setTag("Application/GVL_ExportHMI/DateMM_HMI",state.getValue(),0);
                                                                console.log("Month: "+state.getValue());}, false );
project.getTag("Year",      state,-1,function(tagName, state){  project.setTag("Application/GVL_ExportHMI/DateYYYY_HMI",state.getValue(),0);
                                                                console.log("Year: "+state.getValue()); }, false );

function DateTime_TimeAlignButton_button_onMouseUp(me, eventInfo)
{
	try{
		// Data ora attuale del client:
		var clientDate = new Date();
		console.log("clientDate: " + clientDate);
		//console.log("clientUTCDate: " + clientDate.toUTCString());

        //console.log(clientDate.getUTCFullYear());
		//console.log(clientDate.getUTCMonth());
		//console.log(clientDate.getUTCDate());
		//console.log(clientDate.getUTCHours());
		//console.log(clientDate.getUTCMinutes());
		//console.log(clientDate.getUTCSeconds());

		project.setTag("Year", clientDate.getUTCFullYear());
		project.setTag("Month", clientDate.getUTCMonth() + 1);
		project.setTag("DayOfMonth", clientDate.getUTCDate());
		project.setTag("Hour", clientDate.getUTCHours());
		project.setTag("Minute", clientDate.getUTCMinutes());
		project.setTag("Second", clientDate.getUTCSeconds());

		// Aggiorno perche' il widget datetime non si aggiorna sempre quando modifichi l'orario della tbox
		window.location.reload(true);
	} catch(e) {
		console.log("Error in DateTime_TimeAlignButton_button_onMouseUp: " + e);
	}
}