// NB: Rettangolo disclaimer e' un po' piu' grande perche' con le dimensioni giuste si vedono lo stesso i bordi degli widget sottostanti!
function Disclaimer_hotspotbtn_onMouseUp(me, eventInfo)
{
    var sysInitialized = project.getTag("Application/GVL_ExportHMI/SysInitialized_HMI", new State());
    if( sysInitialized ) {
        project.setTag("Application/GVL_ExportHMI/HideDisclaimer_HMI", 1);
    }
}