# LEGENDA GRAFICHE #

- "Custom Line" è la grafica in "carta millimetrata blu", tipo CL120/CL106 e Navetta30
  Logo selezionabile tra FERRETTI e CUSTOM LINE

- "Ferretti New" è la grafica Ferretti "simil-Loop", tipo F550/F720

- "Ferretti Old" è la grafica Ferretti "family feeling", tipo F850/F920 e Riva.
  Logo selezionabile tra FERRETTI e RIVA

- "Pershing" è la grafica Pershing attuale, tipo 7x/8x/9x

- "Standard" è per il progetto motori standalone, con grafica ispirata ad AZ27

# NOTE #

Necessarie le librerie Naviop e Navico da qui:
H:\Area_Tecnica\Software\000_NAVIOP_SW\CODESYS\v3.5\CodesysV3.5.12\Libraries

# TODO #

ND_standard:    - inserire pagina row
		- Pulsante? per trasferire orario client -> server opbox (per allineare gli orari degli allarmi).
		  Riferimento RD200 Rel 4.00.08 (attenzione, questa funzione è rotta nell'ultima rel al momento, 2022-03-03).

CS:             - troncare il consumo L/h ad una sola cifra decimale (in modo che la somma in NDesign non sia mai arrotondata nel caso mostri 1 cifra decimale)
                - prendere le unità di misura dagli MFD


**************************************************************************
# STORICO RELEASE #

--------------------------------------
| RD201 - OpBox Motori - Rel 1.03.03 |
--------------------------------------
CS:
	- [todo] Trasferimento dei parametri tra 191 e 192.

ND:
	- Fix cambio tema
	- [todo] Allineamneto delle impostazioni.
	- Pulsante di allineamneto dell'orario.
	- [todo] Tutti i client devono fare il reload quando viene settato l'orario dell'opbox in questo modo.
	- Disclaimer è diventato una svg per poter andare accapo...
	- [todo] Verif pagine allarmi, spec js (workaround tabelle ecc)

Elena Maltoni 2022.03.08

--------------------------------------
| RD201 - OpBox Motori - Rel 1.02.07_quit |
--------------------------------------
CS:
	- Aggiornato libreria Naviop a 3.5.12.104 (aggiunto gestione master ethenret per invio su CAN del messaggio Quitt)
	- Quit reset time -> 90ms

Elena Maltoni 2022.03.03

--------------------------------------
| RD201 - OpBox Motori - Rel 1.03.02 |
--------------------------------------
ND Standard:
	- Mergiato js del LoopCore (rel.1.01.01), quindi:
		Ereditato il suo comerror (non servono più le pagine di comerror);
		Aggiunto lettura parametri url; per ora non sono usati, si può usare per interpretare "?loadPage=";
	- Aggiunto abilitazione tasto "check alarms" nel setup.
	- Ridotto le opzioni di filtraggio per lo storico allarmi, altrimenti le ultime vengono tagliate nella combobox.
	- Fix impostazione lingua, spec. all'avvio (riavviando l'MFD rimaneva la selezione "it" ma non veniva impostata la lingua). La lingua è comune a tutti i client, ma nel caso di puntamenti a OPBOX diverse (esempio Navetta30) non esiste ancora un allineamento, quindi bisogna impostare la lingua in un display a 191 e uno a 192.
	- Fix cambio tema; il tasto Show del popup allarmi mandava sempre alla pagina day. L'impostazione del tema è locale al client, si resetta al riavvio del progetto. Modifica non finita, non funziona.

CD:
	- Aggiunto abilitazione tasto "check alarms" nei parametri.

Elena Maltoni 2022.03.03


--------------------------------------
| RD201 - OpBox Motori - Rel 1.03.01 |
--------------------------------------
ND CustomLine (ho aperto il progetto sbagliato...):
	- Impostato target corretto, ovvero OPBOX.
	- Pagina nativa OPBOX rifatta in quanto bacata, probabilmente a seguito di conversioni del progetto.
	- Già che c'ero ho aggiunto una combobox per impostare lo stile grafico... Ovviamente impostando questo parametro cambia solo il CAL, visto che i progetti grafici sono separati.
	- Pagina home, tolto link invalido a tag HideLoading, residuo di qualche Bolt.

ND Standard:
	- Importato pagina nativa OPBOX da ND CustomLine...
	- Pulito dizionari.
	- Riparato workaround per scroll degli allarmi.

ND & CS:
	- Workaround temporaneo per il problema del popup degli allarmi del NOS 64.X.Y.121.
	  PopupWorkaround_Cmd_HMI fa apparire un allarme bianco fasullo per circa un secondo (abbastanza a lungo da far apparire il popup di allarme dell'MFD).
	  Quando il comando viene azzerato, HMI da un secondo Ack agli allarmi.

Elena Maltoni 2022/02/23

--------------------------------------
| RD201 - OpBox Motori - Rel 1.03.00 |
--------------------------------------
Per la release ufficiale manca aggiornare la libreria Naviop

CS:
	- aggiunto gestione master ethenret per invio su CAN del messaggio Quitt
ND:
	- apportato alcune fix grafiche

Lorenzo Asciutti 17/02/2022

--------------------------------------
| RD201 - OpBox Motori - Rel 1.02.07 |
--------------------------------------
CS:
	- Sistemato bug nella gestione del Master Ethernet, per il quale l'opbox stessa si vedeva in timeout Error anche se era l'unica in rete.
ND:
	- Aggiornato il numero di Rel. di tutte le grafiche.

Marco Bertani ft. Stefano Maltoni, 05/10/2021

--------------------------------------
| RD201 - OpBox Motori - Rel 1.02.06 |
--------------------------------------
CS:
	- Sistemato bug nella gestione del Master Ethernet, per il quale l'array nei candidati rimaneva a lunghezza 1, e tutte le OpBox diventavano master
	- Sistemato un bug per il quale se una OpBox è Master, non elimina dalla lista una OpBox che va offline

Marco Bertani, 23/09/2021

--------------------------------------
| RD201 - OpBox Motori - Rel 1.02.05 |
--------------------------------------
CS:
	- aggiunto controllo nel pageloader trip computer, se la sog < 2 per almeno 2 secondi allora non mostro i dati del trip computer calcolati con la sog (Autonomia e Consumo)

Lorenzo Asciutti, 17/09/2021

--------------------------------------
| RD201 - OpBox Motori - Rel 1.02.04 |
--------------------------------------
Rimossa cartella "src/Libraries", non aveva più senso di esistere. Prendete le librerie dal server:

ND_Ferretti_old:
	- rimosso l'header fisso con logo Riva dalla pagina storico allarmi

CS:
	- aggiornato alla libreria Naviop 3.5.12.301

Marco Bertani, 09/09/2021

--------------------------------------
| RD201 - OpBox Motori - Rel 1.02.03 |
--------------------------------------
ND:
	- BIG CHANGE:
	  LA PAGINA SERVICE ORA CONTIENE SOLO I DATI ESSENZIALI
	  LA VECCHIA PAGINA SERVICE CON TUTTI I DATI E' SOTTO PASSWORD
	- sistemati alcuni errori di grafica nelle grafiche ferretti
	- allineato funzionamento (messaggi errore - sensor error) a tutte le grafiche

Lorenzo Asciutti, 03/09/2021

--------------------------------------
| RD201 - OpBox Motori - Rel 1.02.01 |
--------------------------------------

CS:     - Modificata la gestione della selezione server WebSocket: quando avevo un solo MFD che poi andava offline, causava continui tentativi di connessione e relativi rallentamenti

ND_Ferretti_old:    - Aggiunti gauges OIL P. e COOLANT T. alla pagina principale

Marco Bertani, 14/07/2021

--------------------------------------
| RD201 - OpBox Motori - Rel 1.02.00 |
--------------------------------------
Come Rel. 1.01.11, RELEASE UFFICIALE

CS:     - Ripristinato blocco UDP Multicast precedente, perché il nuovo dava dei problemi.
          (Si risolve con una modifica alla libreria)

ND_Ferretti_old:    - Aggiunto il tag AlarmResetCmd_HMI al poulsante di ACK
ND_Pershing:        - Aggiunto il tag AlarmResetCmd_HMI al poulsante di ACK

Marco Bertani, 01/07/2021

--------------------------------------
| RD201 - OpBox Motori - Rel 1.01.11 |
--------------------------------------
CS:     - La sorgente del CAL ora è OpBox-191 (qualunque sia l'indirizzo ip)
        - modifiche al blocco Navico per supportare il Discovery
        - modifche all'EthMasterMng per gestire le accensioni a ethernet staccato (ifconfig non dà info se l'eth è staccato)
        - Fix generali da SQA

ND:     - Fix generali da SQA

--------------------------------------
| RD201 - OpBox Motori - Rel 1.01.10 |
--------------------------------------
CS:    - sistemato il bValidatorByte per il Total Fuel

ND:    - allineati allarmi e tempi dei tag

ND_Ferretti_old:    - scalate lancette giri a 3000 invece che 1000
ND_CustomLine:      - creazione

--------------------------------------
| RD201 - OpBox Motori - Rel 1.01.09 |
--------------------------------------
CS:     - Ora quando "disabilito" un dato da setup, diventa "NOT AVAILABLE" invece che sparire
        - cambiata gestione dei valori "binari" di AuxMAN Value 1: ora danno un AV e un RF più coerente con la logica degli altri dati
        - Ora la SOG dal Trip Computer dà RF = 1 quando non viene letta

ND:     - migliorate traduzioni allarmi
        - aggiunti allarmi SPN 3251, 3285, 3697, 516107
        - cambiata gestione dei valori "binari" di AuxMAN Value 1
        - aggiungere tutti i dati del PGN FF11 "AuxMAN Pressure 2"
        - Cambiata la dicitura "Water Level Fuel Filter" in "Water Level Fuel PREFILTER"
ND_web:	            - migliorate traduzioni nelle pagine
ND_Pershing:        - verificato che le label corrispondano col nome effettivo del dato
ND_Ferretti_old:    - verificato che le label corrispondano col nome effettivo del dato
ND_Ferretti_new:    - verificare che le label corrispondano col nome effettivo del dato

--------------------------------------
| RD201 - OpBox Motori - Rel 1.01.08 |
--------------------------------------
CS:	- Ora quando arriva un dato invalido o non mi arriva del tutto, diventa "NOT AVAILABLE" invece che sparire
        - rivisto il pageloader dei motori per correggere i bargraph


ND:     - Ora quando arriva un dato invalido o non mi arriva del tutto, diventa "NOT AVAILABLE" invece che sparire
ND_MFD: - aggiornata la pagina service alla nuova logica
ND_web:	- Sistemato BG carico motore che andava da 0 a 1000 invece che 0-100
        - Pagina Service: fuel rate impostato senza virgola

--------------------------------------
| RD201 - OpBox Motori - Rel 1.01.07 (NON INSTALLARE IN BARCA!) |
--------------------------------------
CS:	- PageLoader Engines:
		- sistemato EndOfScale Pressione Trasmissione da 10 a 30
		- Aggiunti bargraph a tutte le temperature gas esausti

ND_web:	- Sistemato BG carico motore che andava da 0 a 1000 invece che 0-100
		- Pagina Service: fuel rate impostato senza virgola

--------------------------------------
| RD201 - OpBox Motori - Rel 1.01.06 |
--------------------------------------
CS:	- Proto MAN iSEA:
		- sistemato Total Engine Hours
		- sistemato Total Engine revolutions
		- sistemato Total DEF
		- sistemato Trip DEF
		- sistemato Total Fuel
		- sistemato Trip Fuel
	- PageLoader Engines:
		- sistemato ECU Battery Voltage
		 -sistemata conversione Exhaust back pressure

--------------------------------------
| RD201 - OpBox Motori - Rel 1.01.05 |
--------------------------------------
CS:		- Sistemata lettura carico motore
		- GraphicsStyle_HMI = 0 ora non manda il CAL

--------------------------------------
| RD201 - OpBox Motori - Rel 1.01.04 |
--------------------------------------
CS:		- Correzioni nel function block del TRIP COMPUTER in base alle prove in barca con Lorenzo Andreani
		- Aggiunto array di OptWord_HMI per eventuali setup

ND:		- Sistemato disallineamento allarmi da SPN 516100 in su
		- Aggiunta possibilità di rimuovere manualmente il dato di "T. Uscita Turbo 1"

ND Ferretti Old:- Rinominato da ND Riva
		- Modificata pagina trip computer per supportare più dati, e in maniera più ordinata
		- Aggiunta possibilità di modificare il logo
		- Aggiunti i pulsanti di TRIP RESET nelle pagine full, perché assenti

ND Ferretti New:- Rinominato da ND F550
		- Sistemato pulsante EXIT dal setup Naviop, che non usciva

--------------------------------------
| RD201 - OpBox Motori - Rel 1.01.03 |
--------------------------------------
ND Riva:	- Disable nei tag del motore DX non erano inseriti
ND Pershing:	- Disable nei tag messo anche nei dati che possono scomparire nelle altre pagine

--------------------------------------
| RD201 - OpBox Motori - Rel 1.01.02 |
--------------------------------------
ND web:		- Aggiunti INJECTION LEAKAGE, WATER IN FUEL FILTER 1, WATER IN FUEL FILTER 2 in grafica e negli allarmi
		- Aggiunta del DISABLE nei vari tag dei motori
ND Pershing:	- Aggiunta del DISABLE nei vari tag dei motori

CS: 	- Correzioni varie al protocollo iSEA. Aggiunta del DISABLE nei vari tag dei motori
	- Aggiunto salvataggio automatico dello stile grafico,

Marco Bertani
26/05/2021

--------------------------------------
| RD201 - OpBox Motori - Rel 1.01.01 |
--------------------------------------
ND Pershing:	- Aggiunti INJECTION LEAKAGE, WATER IN FUEL FILTER 1, WATER IN FUEL FILTER 2 in grafica e negli allarmi
		- Aggiunto trasferimento data e ora su OpBox
		- Rimossa icona Naviop nel precaching
		- Aggiunte marce nella pagina principale
		- Aggiunta IMPOSTAZIONE TAG GraphicsStyle_HMI a 3 per Pershing

CS: 	- Aggiunti INJECTION LEAKAGE, WATER IN FUEL FILTER 1, WATER IN FUEL FILTER 2 negli allarmi. Limitato MaxNrOfJ1939KnownAlarms a 78 per fare spazio ai 6 allarmi nuovi
	- Aggiunta gestione icona per Riva


Marco Bertani
27/04/2021

--------------------------------------
| RD201 - OpBox Motori - Rel 1.01.00 |
--------------------------------------
ND:	- Aggiunta grafica Pershing
CS: 	- Aggiunta lettura gasolio da UDP
	- Aggiunta lettuta SOG da WebSocket
	- Invio CAL diversi in funziona del progetto grafico installato
	- Aggiunto scan continuo delle interfacce di rete a intervalli regolari

Marco Bertani
23/04/2021

--------------------------------------
| RD201 - OpBox Motori - Rel 1.00.03 |
--------------------------------------
CS: 	- Aggiunta gestione ridondanza

Marco Bertani
25/03/2021

--------------------------------------
| RD201 - OpBox Motori - Rel 1.00.02 |
--------------------------------------
CS: 	- Abilitato il CAL
	- Leggo IP e vari da Runtime
ND_web: Prima release

Marco Bertani
23/03/2021

--------------------------------------
| RD201 - OpBox Motori - Rel 1.00.01 |
--------------------------------------
Cambiato da RD204 a RD201

CS: 	corretto moltiplicatore della manetta da 0.04 a 0.4
ND_web: aggiunte pagine Riva in lavorazione

Marco Bertani
24/02/2021

--------------------------------------
| RD201 - OpBox Motori - Rel 1.00.00 |
--------------------------------------
Release

Marco Bertani
24/02/2021
